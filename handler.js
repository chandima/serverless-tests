'use strict';

module.exports.hello = async (event) => {
  console.log(JSON.stringify(event.headers, null, 2));
  return {
    statusCode: 200,
    headers: {
      "Access-Control-Allow-Origin" : event.headers.origin,
      "Access-Control-Allow-Credentials" : true,
      "Access-Control-Allow-Methods": "OPTIONS,GET",
      "Set-Cookie": "token=sometoken; Domain=edpl.us; Path=/; SameSite=Lax"
    },
    body: JSON.stringify({
      message: 'Go Serverless! Your function executed successfully!',
      input: event,
    }, null, 2),
  };

};
